<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->uuid('user_id');
            $table->foreign('user_id')->on('users')->references('id');;
            $table->foreignId('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreignId('payment_frequency_id')->nullable();
            $table->foreign('payment_frequency_id')->references('id')->on('payment_frequencies');
            $table->string('title', 250);
            $table->text('description')->nullable();
            $table->float('price');
            $table->string('status');
            $table->string('person');
            $table->date('end_date')->nullable();
            $table->longText('iban')->nullable();
            $table->boolean('is_active')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
