<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\PaymentFrequencyController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\PlanController;
use App\Http\Controllers\Api\V1\CategoryController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);


Route::get('paymentfrequency', [PaymentFrequencyController::class, 'index']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('user', [AuthController::class, 'user']);

    Route::group(['prefix' => '/plans'], function () {
        Route::get('/', [PlanController::class, 'index']);
        Route::post('/store', [PlanController::class, 'store']);
        Route::post('/update/{plan}', [PlanController::class, 'update']);
        Route::post('/delete/{plan}', [PlanController::class, 'destroy']);
        Route::get('/complete/{plan}', [PlanController::class, 'complete']);
    });

    Route::group(['prefix' => '/categories'], function () {
        Route::get('/', [CategoryController::class, 'index']);
        Route::post('/store', [CategoryController::class, 'store']);
        Route::post('/update/{category}', [CategoryController::class, 'update']);
    });
});
