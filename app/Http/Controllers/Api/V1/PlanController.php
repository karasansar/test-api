<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Plan::class);

        $limit = $request->get('limit', 8);
        $isActive = $request->get('isActive', true);

        $plans = Plan::query()
            ->with(['category', 'paymentFrequency', 'user'])
            ->active($isActive)
            ->forAuthenticatedUser()
            ->latest()
            ->paginate($limit);

        return response()->json(['success' => true, 'data' => $plans]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $this->authorize('create', Plan::class);

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'person' => 'required',
            'price' => 'required',
            'status' => 'required',
            'end_date' => 'after_or_equal:'.date('Y-m-d'),
            'category_id' => 'numeric|exists:categories,id',
            'payment_frequency_id' => 'numeric|exists:payment_frequencies,id',
        ]);
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $plan = new Plan();
        $plan->title = $request->title;
        $plan->description = $request->description;
        $plan->price = $request->price;
        $plan->person = $request->person;
        $plan->status = Plan::STATUS_TYPES[$request->status];
        $plan->end_date = $request->end_date;
        $plan->payment_frequency_id = $request->payment_frequency_id;
        $plan->category_id = $request->category_id;
        $plan->iban = $request->iban;
        $plan->user_id = auth()->id();
        $plan->save();

        $plan->load([
            'category',
            'paymentFrequency',
            'user'
        ]);

        return response()->json(['success' => true, 'data' => $plan]);
    }

    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     */
    public function show(Plan $plan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return JsonResponse
     */
    public function update(Request $request, Plan $plan)
    {
        $this->authorize('update', $plan);

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'person' => 'required',
            'price' => 'required',
            'status' => 'required',
            'end_date' => 'after_or_equal:'.date('Y-m-d'),
            'category_id' => 'numeric|exists:categories,id',
            'payment_frequency_id' => 'numeric|exists:payment_frequencies,id',
        ]);
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $plan->title = $request->title;
        $plan->description = $request->description;
        $plan->price = $request->price;
        $plan->person = $request->person;
        $plan->status = Plan::STATUS_TYPES[$request->status];
        $plan->end_date = $request->end_date;
        $plan->payment_frequency_id = $request->payment_frequency_id;
        $plan->category_id = $request->category_id;
        $plan->iban = $request->iban;
        $plan->user_id = auth()->id();
        $plan->save();

        return response()->json(['success' => true, 'data' => $plan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return JsonResponse
     */
    public function destroy(Plan $plan)
    {
        $this->authorize('delete', $plan);

        if (!$plan) {
            return response()->json(['success' => false, 'message' => 'Sonuç Bulunamadı']);
        }
        $plan->delete();

        return response()->json(['success' => true, 'message' => 'İşleminiz Başarıyla Gerçekleştirildi!']);
    }

    public function complete(Plan $plan)
    {
        $this->authorize('update', $plan);

        if (!$plan) {
            return response()->json(['success' => false, 'message' => 'Sonuç Bulunamadı']);
        }
        $plan->update([
            'is_active' => false
        ]);

        $plan->load([
            'category',
            'paymentFrequency',
            'user'
        ]);

        return response()->json(['success' => true, 'data' => $plan]);
    }
}
