<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::query()->get();

        Category::create([
            'name' => 'Netflix',
            'user_id' => $users->random(1)->first()->id
        ]);
        Category::create([
            'name' => 'Türk Telekom',
            'user_id' => $users->random(1)->first()->id
        ]);
    }
}
