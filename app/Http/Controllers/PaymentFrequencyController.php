<?php

namespace App\Http\Controllers;

use App\Models\PaymentFrequency;
use Illuminate\Http\Request;

class PaymentFrequencyController extends Controller
{
    public function index() {
        $paymentFrequency = PaymentFrequency::all();
        return response()->json($paymentFrequency);
    }
}
