<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use HasFactory;
    use SoftDeletes;

    const STATUS_TYPE_PAYEE = 'payee';
    const STATUS_TYPE_PAYER = 'payer';

    const STATUS_TYPES = [
        self::STATUS_TYPE_PAYEE => 'Alacaklı',
        self::STATUS_TYPE_PAYER => 'Borçlu'
    ];


    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function paymentFrequency()
    {
        return $this->belongsTo(PaymentFrequency::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeForAuthenticatedUser(Builder $builder)
    {
        return $builder->where('user_id', auth()->id());
    }

    public function scopeActive(Builder $builder, $isActive = true)
    {
        return $builder->where('is_active', $isActive);
    }
}
