<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = User::query()->where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Auth Api Token')->accessToken;

                return response(['success' => true, 'token' => $token]);
            } else {
                return response()->json(['success' => false, 'message' => 'Şifreler Uyuşmuyor'], 422);
            }
        }

        return response()->json(['success' => false, 'message' => $request->email.' ait kullanıcı kullanıcı bulunamadı!']);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'c_password' => 'required|same:password|min:8',
        ]);
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $token = $user->createToken('Auth Api Token')->accessToken;

        return response()->json(['success' => true, 'token' => $token, 'data' => $user]);
    }

    public function logout(Request $request) {
        $user = $request->user();
        if (Auth::attempt(['email' => $user->email, 'password' => $user->password])) {
            $user->token()->revoke();

            return response(['success' => true, 'message' => 'Başarıyla çıkış yapıldı!']);
        }

        return response(['success' => false, 'message' => 'Çıkış yapılamadı!'], 419);
    }

    public function user()
    {
        if (Auth::check()) {
            $user = Auth::user();

            return response()->json(['success' => true, 'data' => $user]);
        }

        return response()->json(['success' => false, 'message' => 'Kullanıcı bilgisi alınamadı!'], 419);
    }
}
