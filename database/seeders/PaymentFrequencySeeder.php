<?php

namespace Database\Seeders;

use App\Models\PaymentFrequency;
use Illuminate\Database\Seeder;

class PaymentFrequencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentFrequency::create([
            'name' => 'Gün'
        ]);
        PaymentFrequency::create([
            'name' => 'Ay'
        ]);
        PaymentFrequency::create([
            'name' => 'Yıl'
        ]);
    }
}
