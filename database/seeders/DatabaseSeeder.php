<?php

namespace Database\Seeders;

use App\Models\PaymentFrequency;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateUserSeeder::class);
       $this->call(CategorySeeder::class);
       $this->call(PaymentFrequencySeeder::class);
    }
}
